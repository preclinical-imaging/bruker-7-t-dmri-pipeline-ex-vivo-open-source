# Guide to quality control and post-processing of the ex vivo DW-MRI mouse brain data  
### Last reviewed: 27.06.2023


>If you have any questions, need help or further information/clarification, please contact me at cristiana.tisca@yahoo.com.


### Running the pipeline
Execute the following command(s). It is recommended to run the DTI model on the first shell or FSL’s DKI model on both shells.  

If you want to run dtifit on the first shell only, or the kurtosis model within FSL on both shells, then you can run: 

`bruker-7-t-dmri-pipeline-ex-vivo-open-source/diffpostproc_pipeline_full.sh <location_of_sample_folder> <exp_no_shell_1> <exp_no_shell2> <exp_no_blipDown> <location_and_name_of_output_folder> --run_dti_shell1`

or  

`bruker-7-t-dmri-pipeline-ex-vivo-open-source/diffpostproc_pipeline_full.sh <location_of_sample_folder> <exp_no_shell_1> <exp_no_shell2> <exp_no_blipDown> <location_and_name_of_output_folder> --run_dki_bothshells`  


An output folder with the given name will automatically be created at the location you specify.

NODDI can be run using CUDIMOT once this stage of the processing has finalised. Note that there are two options for axial diffusivity (dax): 

* literature-based (example6): 0.56 x 10-3 mm<sup>2</sum>/s 
* data-driven estimate (obtained from ex vivo dMRI data acquired at WIN using the protocol on which this pipeline is based, by running NODDI-SMT - spherical mean technique (Kaden et al, 2016, doi: 10.1016/j.neuroimage.2016.06.002) - iteratively. SMT can provide voxel-wise estimates of dax. The optimised dax value is the mode of the distribution of dax across the corpus callosum, the largest white matter tract): 0.47 x 10-3 mm2/s. We recommend using the optimised dax value. 

If you are interested in extracting other kurtosis metrics (mean kurtosis, radial kurtosis or fractional anisotropy of kurtosis), you can run this command: 

`bruker-7-t-dmri-pipeline-ex-vivo-open-source/extract_kurtosis_metrics.sh <path_to_data_directory>`

The relevant files will be created in the `dtifit_gibbs_eddy_kurtdir` directory (mean kurtosis – MK, radial kurtosis – RK, fractional anisotropy of kurtosis – FAK).  


---
### Output files and folders

Note: only files relevant for QC and data analyses are listed. 

* `data_merged_original.nii.gz`: all volumes acquired during both shells, merged together (should have 64 volumes) 
* `data.nii.gz`: data correctly reoriented 
* `data_gibbs.nii.gz`: Gibbs ringing-corrected data 
* `data_gibbs_eddy.nii.gz`: Eddy- and Gibbs ringing-corrected data 
* `dtifit_gibbs_eddy_firstshell`: Folder containing outputs of FSL’s DTIfit ran on the first shell (FA, MD, V1, etc) 
* `dtifit_gibbs_eddy_kurtdir`: Folder containing outputs of FSL’s diffusion kurtosis model ran on both shells (FA, MD, V1, K1 etc) 
* `data_noddi_bpx.bedpostX`: Folder containing outputs of bedpostX  
* `data_noddi_bpx.NODDI_Watson_diff_exvivo`: Folder containing outputs of NODDI (mean_fintra, mean_fiso, OD) 


---
### Basic Quality Control
Check all above files and directory are present in the directory you created. Check size of data.nii.gz is as expected. 
Inspect the outputs listed above in FSLeyes, ensuring that each processing step was successfully completed and that no major artifacts are persent. 

**Aspects to look out for:**
* Ensure that the signal is homogeneous across the imaging volume (Figure 1A: Raw magnitude data)
* Ensure that 3D Gibbs-ringing correction worked appropriately and that there is no ringing present in any of the dimensions (Figure 1B: Gibbs-ringing corrected data). The red insert in Figure 1A and 1B shows how the Gibbs ringing is reduced in the axial plane in an area of the cortex following the correction step.
* `eddy` uses the outputs from topup and has a two-fold purpose: to correct for susceptibility-induced distortions and to register all volumes (which can most easily be observed when looking at the b=0 volumes before and after eddy-correction, which are usually well aligned after eddy correction but can present slight misalignments before due to signal drift). The correction is normally very subtle, and should not bring about significant alterations to GM/WM shape. Ensure that the signal intensity across b=0 volumes (if you acquired multiple volumes) is similar across the volumes (Figure 1 C: Gibbs- and eddy-corrected data), and the contrast in the diffusion-weighted volumes looks sensible. Ensure that the `FSL eddy` registration worked well. The blue insert in Figure 1B and 1C shows the subtle differences observed in the area between the cerebellum and cortex following eddy correction. Air microbubbles are often present in that area, creating susceptibility differences. 



<img src="Photos/Fig1_Guide_QC_Raw_Gibbs_Eddy_Corr.png" alt="Figure 1. First three stages of the pipeline."
	title="Figure 1. First three stages of the pipeline." width="850" height="600" align="centre" /></p>

<b> Figure 1. First three stages of the pipeline </b>





The following gif shows the how the data should look like before and after eddy correction, provided that only a small amount of air microbubbles are present on the cortical surface:


<img src="Photos/Before_after_eddy_correction2.gif" alt="Figure 2. Eddy correction gif"
	title="Figure 2. Before and after eddy correction." width="600" height="180" align="centre"/>

<b> Figure 2. Before and after eddy correction. </b>




* DTIfit/NODDI: outputs: Ensure that there is uniformity across the brain. Once the pipeline has been run on all samples, compare the FA/MD/etc values between mice from your cohort to spot any obvious outliers. Below is an example FA map: 

<img src="Photos/FA_map.png" alt="Figure 4. Example FA image."
	title="Figure 3. Example FA image."  width="500" height="200" />

<b> Figure 3. Example FA image. </b>

* When evaluating the outputs from DTI, check that the V1 directions are anatomically accurate and colour-coded as expected (as in the figure below, showing an example V1 map overlayed on FA). Select tracts known to run left-right, anterior-posterior and superior-inferior and confirm that the V1 map shows the expected tract orientation. E.g., the corpus callosum is expected to connect the two hemispheres, with tracts running left-right in the coronal plane (Fig 4B, red); the horns of the anterior commissure are expected to run in the anterior-posterior direction (Figure 4C, green); and the majority of tracts in the cerebellum are expected to run in the superior-inferior direction (Figure 1A, blue). If this is not the case, there is potentially an issue with the rotations applied to the b-vectors used in the analysis and that needs to be corrected.


 <img src="Photos/V1_overlayed_on_FA.png" alt="Figure 5. Example V1 file displayed in FSleyes."
	title="Figure 4. V1 file displayed in FSleyes." width="800" height="450" />

<b>Figure 4. V1 file displayed in FSLeyes.</b>

Example NODDI OD map:

<img src="Photos/OD_map.png" alt="Figure 6. Example OD image."
	title="Figure 5. Example OD image." width="500" height="200" />

<b>Figure 5. Example OD map. </b>


* The pipeline generates a significant number of files which are not normally needed for higher-order analysis, taking up about ~6-7GB/sample. We have a cleanup script for the diffusion folder, to get rid of some of the intermediary files. This can be done after the initial QC step. This brings your folder size down to ~250MB. It can be run like the other shell scripts, using:  

`bruker-7-t-dmri-pipeline-ex-vivo-open-source/diff_cleanup.sh <folder_name>`







