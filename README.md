# WIN Bruker 7T Ex Vivo Diffusion-Weighted MRI Post-Processing Pipeline 

This repository contains all scripts to run the ex vivo diffusion-weighted MRI (dMRI) post-processing pipeline on data acquired at WIN's 7T Bruker facility. It should be compatible with any other data acquired on a similar Bruker scanner and using an equivalent protocol.

This resource contains anonymised file-paths which will need to be edited to enable running on a cluster facility. The commands for submitting jobs to the cluster also need to be edited.

This post-processing pipeline was co-deveveloped by Aurea Martins-Bach (aurea.martins-bach@ndcn.ox.ac.uk) and Cristiana Tisca.

The outputs of the pipeline include the standard DTI (FA, MD, V1) and DKI (FA, AK, RK, K1) outputs from FSL and the standard NODDI outputs (OD, ICVF, ISOVF) from cuDIMOT.

The code is maintained by Cristiana Tisca.

Cristiana Tisca
cristiana.tisca@linacre.ox.ac.uk, cristiana.tisca@yahoo.com
June 2023

---

### HOW TO CITE THIS MATERIAL
DOI: 10.5281/zenodo.8129321

---

### SOFTWARE REQUIREMENTS

* `FSL` (`topup`, `eddy`, `CUDIMOT`, `FSLeyes`)
* `mrdegibbs3D` (https://github.com/jdtournier/mrdegibbs3D)

---

### NOTES ON RUNNING THE PIPELINE

* Running `topup`
Note image dimensions need to be swapped for this step as our protocol has the phase encode direction in the z direction (superior-inferior direction). Topup has an inbuilt sanity check which does not allow us to specify that the phase encode direction is z, because in an in vivo human scan such an acquisition wouldn't be practical due to aliasing. The swapping step is done to bypass this check.

* Running `bedpostX` and `NODDI`
BedpostX and NODDI don't take jobids so they can only be run once previous jobs have finished.

* Running NODDI
Axial diffusivity (`dax`) value optimised in an iterative fashion on data acquired using our exvivo dMRI protocols using NODDI, in combination with the spherical mean technique (SMT), which generates voxel-wise estimates for axial diffusivitiy.

---

### NOTES ON DATA ACQUISITION

This pipeline makes, at various points, assumptions about the format of the data (e.g. the `topup` `acp.txt` and `index.txt` files, the specific orientation correction done for the nifti files). Please adjust these files depending on your requirements. See below for a list of adjustments that need to be made to run the pipeline on your cluster.

Our multi-modal ex vivo MRI data acquisition parameters (including dMRI, multi-echo GRE and structural T2-weighted MRI) can be found in `acq_params.md`.

---
### DIAGRAM OF THE PIPELINE

<p align="center">
<img src="Pipeline_Guide_and_QC/Photos/Fig4.6.Diffusion_pipeline.png" alt="Diagram of the diffusion-weighted MRI post-processing pipeline"
	title="Diagram of the diffusion-weighted MRI post-processing pipeline" width="700" height="400" />

Diagram of the dMRI post-processing pipeline. b-vecs: b-vectors; b-vals: b-values;  $\chi$ : susceptibility;  FA: fractional anisotropy; MD: mean diffusivity; AK: apparent kurtosis; OD: orientation dispersion;  ICVF: intra-cellular volume fraction; ISOVF: isotropic volume fraction.
</p>
---

### PUBLISHED WORK USING THIS PIPELINE


* OHBM Annual Meeting. Cristiana Tisca, Mohamed Tachrount, Adele Smart, Frederik J Lange, Amy FD Howard, Chaoyue Wang, Lily Qiu, Benjamin Tendler, Claire Bratley, Daniel ZL Kor, Istvan N Huszar, Javier Ballarobre-Barreiro, Marika Fava, Manuel Mayr, Jason P Lerch, Aurea B Martins-Bach, Karla L Miller. Linking MRI to histology in the mouse brain: A framework for data acquisition and pre-processing. July 2023
* ISMRM & ISMRT Annual Meeting. Aurea B Martins-Bach, Cristiana Tisca, Mohamed Tachrount, Carmelo Milioto, Mireia Carcolé, Shoshana Spring, Remya R. Nair, Thomas J. Cunningham, Elizabeth M. C. Fisher, Adrian M. Isaacs, Brian J. Nieman, Jason Lerch, and Karla Miller. Distinct patterns of microstructural changes in mouse models of ALS/FTD with mutations in Tardbp, Fus and C9orf72. June 2023
* ISMRM & ESMRMB Joint meeting. Cristiana Tisca, Mohamed Tachrount, Frederik J Lange, Amy FD Howard, Chaoyue Wang, Lily R Qiu, Benjamin C Tendler, Jason P Lerch, Aurea B Martins-Bach, Karla L Miller. White matter microstructure changes in a Bcan knockout mouse model. May 2022 
* Brain and Brain PET Annual Conference. Kamila U Szulc-Lerch, Eugenio Graceffo, Cristiana Tisca, Aurea B Martins-Bach, Karla L Miller, Clemence Ligneul, Jason P Lerch, Yvonne Couch, Tracy Farr, Heidi Johansen-Berg (2022). Mapping acute stroke in the mouse using structural and diffusion MRI. May 2022
* BRAIN Conference. Kamila Szulc-Lerch, Eugenio Graceffo, Lily Qiu, Cristiana Tisca, Aurea B Martins-Bach, Karla L Miller, Mohammed Tachrount, Claire Bratley, Clémence Ligneul, Antoine Cherix, Jason Lerch, Yvonne Couch, Farr Tracy, Heidi Johansen-Berg. Multi-modal MRI Characterisation of a Mouse Model of Distal Middle Cerebral Artery Occlusion. February 2022
* ISMRM & SMRT Virtual Conference. Aurea B Martins-Bach, Mohamed Tachrount, Cristiana Tisca, Lily Qiu, Shoshana Spring, Jacob Ellegood, Brian J Nieman, John G Sled, Remya Raghavan-Nair, Elizabeth Fisher, Thomas Cunningham, Jason Lerch, Karla Miller. Anatomical and microstructural brain alterations in the TDP-M323K mouse model of amyotrophic lateral sclerosis. Apr 2021
* ISMRM & SMRT Virtual Conference. Cristiana Tisca, Mohamed Tachrount, Chaoyue Wang, Lily Qiu, Javier Barallobre-Barreiro, Marika Fava, Manuel Mayr, Jason Lerch, Aurea B Martins-Bach, Karla L Miller. Vcan mutation leads to sex-specific changes in white matter microstructure in mice. Apr 2021


---

### WIN RODENT EX VIVO TWO-SHELL DIFFUSION DATA ACQUISITION DETAILS

* the diffusion data consists of three separate acquisitions: 1 volume acquired using reverse-phase encoding direction (‘Blip Down’), followed by an acquisition of 32 volumes at b=2,500mm2/s with b=0 volumes interspersed equally (acquired in the same phase-encoding direction as the other volumes, ‘Blip Up’) (1 x b=0 mm2/s, 15 x b=2,500mm2/s, 1 x b=0mm2/s, 15 x b=2,500mm2/s) and an acquisition of 32 volumes at b=10,000mm2/s with b=0 volumes interspersed equally (1 x b=0 mm2/s, 15 x b=10,000mm2/s, 1 x b=0mm2/s, 15 x b=10,000mm2/s) 

* additional details about scanning parameters: segmented EPI sequence, 12 segments, TE=30ms, TR=500ms, delta/Delta=6.7/13.5ms, gradient directions covering the whole shell and staggered between shells, BW=250kHz 

* all data is initially corrected for Gibbs ringing, using a 3D Gibbs-ringing correction method (https://github.com/jdtournier/mrdegibbs3D) 

* the Blip-Down volume is used together with the first b=0 volume of the b=2,500s/mm2 shell and a custom configuration file to correct for susceptibility-induced distortion using FSL topup, and the output is fed into FSL eddy which corrects the distortions and aligns all volumes 

* the corrected volumes are fed into FSL’s DTIFIT to produce principal eigenvector of the diffusion tensor (V1) maps, fractional anisotropy (FA) and mean diffusivity (MD) maps and into the cuDIMOT5 tool to produce NODDI parametric maps: orientation dispersion index (OD), intra-cellular volume fraction (fintra, also known as neurite density fraction - NDI) and extra-cellular volume fraction (fiso) 

---

### ADJUSTMENTS NEEDED TO RUN THE PIPELINE ON YOUR LOCAL CLUSTER

*  `masterfiles/acp.txt`, `masterfiles/index.txt`, `masterfiles/index_oneshell.txt` have to be edited for topup/eddy depending on the characteristics of your dataset (number of volumes etc). For instructions on how to do this look here: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/TopupUsersGuide, https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy/UsersGuide 
*  WIN's cluster runs using the Grid Engine (GE) queuing software (using the Son of Grid Engine distribution). To ease job submission we use a helper called fsl_sub​ which sets some useful options to Grid Engine's built-in qsub​ command. You will need to change/edit out all the job submission lines depending on your cluster's requirements.
* We perform a reorientation of the dimensions nifti files into correct rodent neuroanatomical dimensions using the function `orient_corr`. You might have to adapt this function based on the orientation of the nifti files outputted by your scanner software.
* `diffpostproc_step1.sh` or `diffpostproc_step1_oneshell.sh` and `bin/extract_bvecs_bvals.sh` or `bin/extract_bvecs_bvals_oneshell.sh` may need to be adapted to account for different sample orientations or a different set of acquisition parameters (ordering of b=0 values, number of b=0 images or number of diffusion directions)
---

### BIBLIOGRAPHY

* Andersson, J. L. R., Skare, S. and Ashburner, J. (2003) ‘How to correct susceptibility distortions in spin-echo echo-planar images: Application to diffusion tensor imaging’, NeuroImage. doi: 10.1016/S1053-8119(03)00336-7.
* Andersson, J. L. R. and Sotiropoulos, S. N. (2016) ‘An integrated approach to correction for off-resonance effects and subject movement in diffusion MR imaging’, NeuroImage. doi: 10.1016/j.neuroimage.2015.10.019.
* Bautista, T. et al. (2021) ‘Removal of Gibbs ringing artefacts for 3D acquisitions using subvoxel shifts’, in Proc. Intl. Soc. Mag. Reson. Med., p. 3535.
* Behrens, T. E. J. et al. (2007) ‘Probabilistic diffusion tractography with multiple fibre orientations: What can we gain?’, NeuroImage, 34(1), pp. 144–155. doi: 10.1016/j.neuroimage.2006.09.018.
* Hernandez-Fernandez, M. et al. (2019) ‘Using GPUs to accelerate computational diffusion MRI: From microstructure estimation to tractography and connectomes’, NeuroImage. doi: 10.1016/j.neuroimage.2018.12.015.
* Kaden, E. et al. (2016) ‘Multi-compartment microscopic diffusion imaging’, NeuroImage. doi: 10.1016/j.neuroimage.2016.06.002.
* Kellner, E. et al. (2016) ‘Gibbs-ringing artifact removal based on local subvoxel-shifts’, Magnetic Resonance in Medicine. doi: 10.1002/mrm.26054.
* Smith, S. M. et al. (2004) ‘Advances in functional and structural MR image analysis and implementation as FSL’, in NeuroImage. doi: 10.1016/j.neuroimage.2004.07.051.
* Zhang, H. et al. (2012) ‘NODDI: Practical in vivo neurite orientation dispersion and density imaging of the human brain’, NeuroImage. doi: 10.1016/j.neuroimage.2012.03.072.
