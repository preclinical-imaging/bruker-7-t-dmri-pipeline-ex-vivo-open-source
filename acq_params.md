### Multi-modal MRI data acquisition parameters

* dMRI acquisition parameters: 100µm isotropic, matrix size: 240x96x120, 3D segmented EPI, 12 segments, TE=30ms, TR=500ms, $\delta/ \Delta$ = 6.7/13.5ms, b=0 s/mm2 (volumes: 4+1 phase-encoding reversed), b=2,500 s/mm2 (volumes: 30), b=10,000 s/mm2 (volumes: 30), BW=250kHz, directions covering the whole shell and staggered between shells. 


* T2-weighted MRI acquisition parameters: 60µm isotropic, matrix size: 60µm isotropic, matrix size: 400x160x200, 3D TurboRARE, TE=12ms, echo spacing=12ms, 6 echoes, TR=350ms, BW=60kHz. 


* Multi-echo GRE acquisition parameters: 100µm isotropic, matrix size: 240x96x120,3D Multi-echo GRE, FA=15&deg;, TE1=3ms, echo spacing=3ms, 20 echoes, TR=68ms, NR=2, BW=250kHz, monopolar readout, prospective navigator correction. 

